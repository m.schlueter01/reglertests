from numpy import min_scalar_type, save, sqrt
from simple_pid import PID
from time import sleep
import matplotlib.pyplot as plt
from multiprocessing import Pool
import random
import json
import numpy

tn = 50
delay = tn // 5

def soll(t,_tn):
    t0 = 0

    if t > t0 and t < _tn:
        #return 1.0
        return (t)/(_tn - t0)
    elif t >= _tn:
        return 1.0
        #return 0.0
    return 0.0

def ist(state, control):
    state["cont"].insert(0,control)
    sum = 0
    for el in state["cont"]:
        sum += el
    state["hist"].insert(0,sum)
    return state["hist"][delay]

def cont_error(val_is, val_soll):
    val_diff = val_soll - val_is
    return val_diff*val_diff

def simulate(params,log = False):
    kp = params["kp"]
    ki = params["ki"]
    kd = params["kd"]

    pid = PID(
        kp, ki, kd, 
        sample_time=0.001, 
        output_limits=(0,1)
        )
    state = { "hist": delay*[0], "cont": [] }

    if log:
        plt_cvis = []
        plt_cont = []
        Kp = []
        Ki = []
        Kd = []
        error = []
        soll_list = []
        plt_real = []
    ist_val = 0
    error_val = 0
    _tn = tn
    for t in range(tn+delay):
        pid.setpoint = soll(t,_tn)
        con = pid(ist_val, dt = 0.001)
        ist_val = ist(state,con)
        t_error = cont_error( state["hist"][0] , pid.setpoint)
        error_val += t_error
        if log:
            plt_cont.append(con)
            plt_cvis.append(ist_val)
            plt_real.append(state["hist"][0])
            Kp.append(pid.components[0])
            Ki.append(pid.components[1])
            Kd.append(pid.components[2])
            error.append(pid.setpoint - ist_val)
            soll_list.append(pid.setpoint)

    error_val += delay*cont_error( state["hist"][0] ,pid.setpoint)
    if(abs(pid.setpoint - state["hist"][0]) > 0.2):
        error_val = 100000

    if log:
        return {"errsum":error_val,"errors":error,"control":plt_cont,"contis":plt_cvis,"Kp": Kp,"Ki":Ki,"Kd":Kd,"real":plt_real,"set":soll_list}
    
    return {"errsum": error_val, "params": params}

def plot_vals(res):
    k_fact = 5.0
    #plt.plot(res["contis"], label="control-vis")
    #plt.plot(res["control"], label="control")
    plt.plot(res["real"], label="real")
    plt.plot([k_fact * v for v in res["Kp"]],label="Kp")
    plt.plot([k_fact * v for v in res["Ki"]],label="Ki")
    plt.plot([k_fact * v for v in res["Kd"]],label="Kd")
    #plt.plot([val for val in res["errors"]],label="error")
    plt.plot(res["set"],label="soll")
    plt.ylabel('is values')
    plt.legend()
    plt.savefig('plt.png')

def crop_param(param, min, max):
    if(param > max):
        return max
    if param < min:
        return min
    return param

def rand_search(n,start_params,epsilon):
    error_min = 100000

    best_param = start_params

    rand_vec = get_parameter_vector(best_param,epsilon,n)

    with Pool(8) as p:
        res_vec = p.map(simulate, rand_vec)
   
    for res in res_vec:
        error = res["errsum"]
        p = res["params"]
        if error < error_min:
            error_min = error
            best_param = p
            print("{0} {1} {2} {3}".format(p["kp"],p["ki"],p["kd"],error))
    
    ve = numpy.var([r["errsum"] for r in res_vec])

    lr = 0.1

    dest_var = 0.01

    if(epsilon["kp"] > 0.00001):
        epsilon["kp"] *= 1.0 - lr * crop_param((ve - dest_var),-1,1)
    if(epsilon["ki"] > 0.00001):
        epsilon["ki"] *= 1.0 - lr * crop_param((ve - dest_var),-1,1)
    if(epsilon["kd"] > 0.00001):
        epsilon["kd"] *= 1.0 - lr * crop_param((ve - dest_var),-1,1)
    
    return best_param

def save_params(params,filename="myparams.txt"):
    a_file = open(filename,"w")
    json.dump(params,a_file)
    a_file.close()

def load_params(filename="myparams.txt"):
    a_file = open(filename,"r")
    content = json.load(a_file)
    a_file.close()
    return content

def get_parameter_vector(params,epsilon,n):
    res = []
    for i in range(n):
        kp = random.gauss(params["kp"],epsilon["kp"])
        ki = random.gauss(params["ki"],epsilon["ki"])
        kd = random.gauss(params["kd"],epsilon["kd"])
        #kp = 0.10064385952264086
        #ki = -0.26548344503961524
        #kd = 7.78214095617029e-05
        res.append({"kp":kp,"ki":ki,"kd":kd})
    return res

def rand_search_auto_window(n=10,start_params=None,epsilon = 0.5,depth = 3,epsilon_reduc = 0.9):
    params = start_params
    for i in range(depth):
        params = rand_search(n = n,start_params = params, epsilon = epsilon)
        epsilon *= epsilon_reduc
        print(i)
    return params

def rand_search_pid(n,start_params, epsilon ):
    params = start_params
    div = 10
    part = n // div
    for i in range(div):
        _epsilon = epsilon.copy()
        if(i % 3 == 0):
            _epsilon["ki"] = 0
            _epsilon["kd"] = 0
            params = rand_search(n = part,start_params = params, epsilon = _epsilon)
            epsilon["kp"] = _epsilon["kp"]
        if((i + 1) % 3 == 0):
            _epsilon["kp"] = 0
            _epsilon["kd"] = 0
            params = rand_search(n = part,start_params = params, epsilon = _epsilon)
            epsilon["ki"] = _epsilon["ki"]
        if((i + 2) % 3 == 0):
            _epsilon["ki"] = 0
            _epsilon["kp"] = 0
            params = rand_search(n = part,start_params = params, epsilon = _epsilon)
            epsilon["kd"] = _epsilon["kd"]

        print("epsilon: {0} {1} {2}".format(epsilon["kp"],epsilon["ki"],epsilon["kd"]))
    return rand_search(n = part,start_params = params, epsilon = epsilon)



params = load_params()
epsilon = {"kp":0.01,"ki":0.1,"kd":0.0001}
#params = None
#params = rand_search_auto_window(n=100000,start_params=params,epsilon=100.0,depth = 10,epsilon_reduc = 0.8)
params = rand_search(n = 1000000,start_params = params, epsilon = epsilon)
#params = rand_search_pid(n = 100000,start_params=params,epsilon=epsilon)
log = simulate(params,log=True)
print(log["errsum"])
plot_vals(log)
save_params(params)